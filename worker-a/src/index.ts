import { Hono } from 'hono'
import { zValidator } from '@hono/zod-validator'
import { z } from 'zod'

const app = new Hono()

const route = app.get('/api/rpc', zValidator('query', z.object({ name: z.string() })), (c) => {
  const { name } = c.req.valid('query')
  return c.json({ message: `Hello via RPC ${name}` })
})

export type AppType = typeof route

export default app
