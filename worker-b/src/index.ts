import { Hono } from 'hono'
import { hc } from 'hono/client'
import type { Fetcher } from '@cloudflare/workers-types'
// @ts-ignore
import type { AppType } from '../../worker-a/src/index'

type Bindings = {
  hello: Fetcher
}

const app = new Hono<{ Bindings: Bindings }>()

app.get('/', async (c) => {
  const client = hc<AppType>('/', { fetch: c.env.hello.fetch.bind(c.env.hello) })
  const res = await client.api.rpc.$get({ query: { name: 'Hono' } })

  const data = await res.json()
  return c.json(data.message)
})

export default app
